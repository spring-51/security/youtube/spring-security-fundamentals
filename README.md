# Spring security by Laurentiu Spilca (You tube)
## DEBUGGING SPRING SEC
```
use below properties to trace logs
logging.level.org.springframework.security.*=debug
```

```
Instrutor : Laurentiu Spilca
Youtube channel : Spring Security fundamental
Link - TODO
```

```
L1 (THE BASIC CONTRACT OF SPRING SECURITY(ARCHITECTURE))
   
   - Default UserDetailsService
   - Creating bean of UserDetailsService (In-memory)
   - refer  -  ProjectSecurityConfig -> userDetailsService
   
Filetr of Filter Chain
- BasicAuthenticationFilter
  -- THIS IS RESPONSIBLE FOR BYDEFAULT SPRING SEC AUTHENTICATION ITS FOR HTTP BASIC AUTHENTICATION


git-repo - 
```

```
L2 (Implemting UserDetailsService)

   - Default UserDetailsService
   - Changing bean of UserDetailsService from In-memory to H2 database table
   - refer  -  ProjectSecurityConfig -> userDetailsService

Imp Notes :    
UserDetailsService vs UserDetailsManager
 - UserDetailsManageris child calss of UserDetailsService
 - UserDetailsService also has api to 
   -- createUser, 
   -- changePassword ,
   -- deleteUser etc


git-repo - 
```

```
L3 (UserDetailsManager and PasswordEncoder)

1.
UserDetailsService vs UserDetailsManager
 - UserDetailsManageris child calss of UserDetailsService
 - UserDetailsService also has api to 
   -- createUser, 
   -- changePassword ,
   -- deleteUser etc

2.
JdbcUserDetailsManager
/**
 * if we uses JdbcUserDetailsManager then we need to follow table names as per
 * defined in JdbcUserDetailsManager refer **JdbcUserDetailsManager**
 */
 i.e for user table name must be "users",
 and for grantedAuthorities table name must be "authorities" etc.
 If we want to use different name then we need to create our own JdbcUserDetailsManager
 
 3. PassowrdEncoder - we chnaged password Encoder here from Noop to bcrypt
 - refer  -  ProjectSecurityConfig -> passwordEncoder
         /**
         * Advantage of using Bcrypt
         * 1. - we can't extract password back from encoded string.
         * 2. - for same password string encoded string is always different
         */

 git
 - we have tagged L3 as it is using JdbcUserDetailsManager
 - checkout theis tag when we want to use JdbcUserDetailsManager as UserDetailsManager(i.e UserDetailsService) 
 - tag
   - from branch "lesson-3-jdbc-user-details-manager"
   - name - "lesson-3"
 instructor-repo - 
```


```
L4 (The AuthenticationProvider)

1. Here we reverted back to InMemoryUserDetailsManager 
  - refer ProjectSecurityConfig -> userDetailsService
  - we can also create user even in in-mem 
    -- refer - UserController -> create , DataLoader -> run
      -- UserDetailsService bean in autowired to userDetailsManager

2. we have creted our own custom AuthenticationProvider
  - CustomAuthenticationProvider implements AuthenticationProvider
    -- AuthenticationProvider(I) has two abstract method
      --- 1. supports()
         ---- this is called by authentication-MANAGER before calling below authenticate() method
      --- 2. authenticate()
  
git
 - tag
   - from branch "lesson-4-custom-authentication-provider"
   - name - "lesson-4"
 instructor-repo -
```

```
L5 (The FilterChain)

1. Added Custom Filter - refer  - CustomAuthenticationFilter
2. Created custom Authentication 
  - refer - CustomAuthentication
  - refer - CustomAuthentication -> doFilter
3. Added  CustomAuthenticationFilter in Spring Security filter chain
  - refer - ProjectSecurityConfig -> configure(HttpSecurity)
4. Created another AP - refer - FromHeaderAuthenticationProvider

5. Config FromHeaderAuthenticationProvider in the Spring security
  - refer - ProjectSecurityConfig -> configure(AuthenticationManagerBuilder)

git
 - tag
   - from branch ""
   - name - ""
 instructor-repo -
```

```
L6 (Part 1 - Multiple authentication provider)
1. In lecture 5 we replaced BasicAuthenticationFilter
   - refer - ProjectSecurityConfig -> configure(HttpSecurity) 
     -- http.addFilterAt(customAuthenticationFilter, BasicAuthenticationFilter.class);

2. Here we used UsernamePasswordAuthFilter to intercept /login "url", we have not creted any controller for /login
  - refer - UsernamePasswordAuthFilter -> shouldNotFilter

new-classes
- Otp
- OtpRepo
- UsernamePasswordAuthFilter
- UsernamePasswordAuthentication

deleted class
- all filters till L5
- all CustomAuthentication till L5
git
 - tag
   - from branch ""
   - name - ""
 instructor-repo -
```

```
L7 (Part 2 - Multiple authentication provider)
git
 - tag
   - from branch ""
   - name - ""
 instructor-repo -
```

```
L8 (Security Context Holder)
[debt from lecture 7]
- from lecture 7 we are getting some cyclinc dependency
  - it was caused when we added AuthenticationManager dependency in TokenAuthenticationFilter (I am noot able to see cyclic dependeny ?)
  - to fix this we created bean of all filters using @Bean , we avoided Stereotype annotations
    -- refer - ProjectSecurityConfig -> usernamePasswordAuthFilter()
    -- refer - ProjectSecurityConfig -> tokenAuthenticationFilter()

2.
We can grab authentication from security context at enywher within our app
 - Controllers: spring automatically inject Authentication  after reading from SecurityContextHolder in our controllers
   -- this auto injection works ONLY within CONTROLLERS
   -- refer - TestController -> test()
 - Other than Controller : in service layer to get Authentication object using SecurityContextHolder
   -- Authentication a = SecurityContextHolder.getContext().getAuthentication()

3. Impact of async/multi-threaded  on SecurityContextHolder strategy -
  - SecurityContextHolder uses strategy as  MODE_THREADLOCAL (refer SecurityContextHolder) by default , it means 
    if we use SecurityContextHolder.getContext().getAuthentication() it will return 
    -- non-null object if we are using same thread
      --- refer -  TestController -> test()
      --- MODE_THREADLOCAL - associate SecurityContextHolder( or ThreadLocal ) to single thread
         ---- for eg if thread "t1" save details to a ThreadLocal then the thread "t1" will ONLY be able to accss saved details from the ThreadLocal 
    -- null object if we are creating another thread and withing that thread job we are calling  SecurityContextHolder.getContext().getAuthentication()
      --- refer -  TestController -> testAsync()
      --- we can get non-null value here simple changing  strategy, from MODE_THREADLOCAL  to MODE_INHERITABLETHREADLOCAL
      --- MODE_INHERITABLETHREADLOCAL - it tell Spring to copy the  information from SecurityContext from parent thread to the child thread 
           -- refer - TestController -> testAsync()
         ---- this will ONLY work for Spring manged child threads(if child thread is created using @Async)
         ---- for user created thread THIS WILL NOT WORK
              [ITS WORKING FOR SpringBoot version 2.4.5]
                -- in some older version in order to fix tis we need to use Delegating classes
                   -- refer - TestController -> testAsync2() [Using  DelegatingSecurityContextRunnable is OPtional for SpringBoot version 2.4.5]
                   -- some other delegating classes are
                     -- DelegatingSecurityContextRunnable
                     -- DelegatingSecurityContextCallable
                     -- DelegatingSecurityContextXxx.. 
4. Changing SecurityContextHolder strategy
  - WAY 1
  - create a bean of type InitializingBean as below
    @Bean
    public InitializingBean initializingBean(){
        return () ->{
            SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);  
    };
    - refer - ProjectSecurityConfig -> initializingBean()
    
    - WAY 2 -
    [NOT WORKING]
    - configuring - spring.security.strategy 
      -- refer - applicatio.properties -> spring.security.strategy 
} 
git
 - tag
   - from branch "lesson-8-multiple-authentication-provider-and-shc"
   - name - "lesson-8"
 instructor-repo -
```

