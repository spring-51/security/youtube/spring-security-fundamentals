package com.example.springsecurity.filter;

import com.example.springsecurity.authentication.UsernameOtpAuthentication;
import com.example.springsecurity.authentication.UsernamePasswordAuthentication;
import com.example.springsecurity.entity.Otp;
import com.example.springsecurity.repo.OtpRepo;
import com.example.springsecurity.repo.inmem.TokenDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Random;
import java.util.UUID;

/**
 * We have created Custom filter by implementing Filter
 * Problem with this is we need to type cast request to HttpServletRequest as
 * HttpServletRequest http = (HttpServletRequest) request;
 *
 * if we want to avoid this we can create this filter by extending OncePerRequestFilter
 *
 * UsernamePasswordAuthFilter extends OncePerRequestFilter{
 *
 * }
 *
 *  as a good practice use OncePerRequestFilter over Filter to create custom filter
 */
//@Component // bean of filter is created using @Bean in ProjectSecurityConfig
public class UsernamePasswordAuthFilter extends OncePerRequestFilter {

    /**
     * AuthenticationManager bean by default is not part of context
     * So we need to create bean of AuthenticationManager type
     * refer - ProjectSecurityConfig -> authenticationManagerBean()
     */
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private OtpRepo otpRepo;

    @Autowired
    private TokenDb tokenDb;

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain) throws ServletException, IOException {
        // step 1 : username and password
        // step 2 : username and otp

        // as a good practice use header to get username and password as its more secure when used with https
        String username = request.getHeader("username");
        String password = request.getHeader("password");
        String otp = request.getHeader("otp");
        if(ObjectUtils.isEmpty(otp)){
            // step 1
            Authentication a = new UsernamePasswordAuthentication(username, password);
            a = authenticationManager.authenticate(a);
            // crete otp save in db and return to the user
            Otp otpEntity = new Otp();
            otpEntity.setUsername(username);
            otpEntity.setOtp(String.valueOf(new Random().nextInt(9999)+1000));
            otpRepo.save(otpEntity);
            response.setHeader("otp", otpEntity.getOtp());
        }else{
            // step 2
            Authentication a = new UsernameOtpAuthentication(username, password);
            a = authenticationManager.authenticate(a);
            String token = UUID.randomUUID().toString();
            tokenDb.add(token);
            response.setHeader("Authorization", token);
        }

    }

    /**
     * allows us to define a rule when we dont want to enable this filter
     * @param request
     * @return
     * @throws ServletException
     */
    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        /**
         * this tells this filter eill not be enabled for uri "/login"
         */
        return !request.getServletPath().equals("/login");
    }
}
