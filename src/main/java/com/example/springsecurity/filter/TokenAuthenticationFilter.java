package com.example.springsecurity.filter;

import com.example.springsecurity.authentication.TokenAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This filter should only be active if we have token
 * it will validate the toke
 */
// @Component // when we added AuthenticationManager as dependency app failing to start , why ?
              // bean of filter is created using @Bean in ProjectSecurityConfig
public class TokenAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain) throws ServletException, IOException {

        String token = request.getHeader("Authorization");
        Authentication a = new TokenAuthentication(token, null);
        a = authenticationManager.authenticate(a);

        SecurityContextHolder.getContext().setAuthentication(a);

        //lastly
        chain.doFilter(request,response);
    }

    /**
     * allows us to define a rule when we dont want to enable this filter
     * this filter is exact opposite of UsernamePasswordAuthFilter
     * will be active for all except /login
     * @param request
     * @return
     * @throws ServletException
     */
    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        return request.getServletPath().equals("/login");
    }
}
