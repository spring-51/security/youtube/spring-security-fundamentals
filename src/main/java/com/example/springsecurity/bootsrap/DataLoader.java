package com.example.springsecurity.bootsrap;

import com.example.springsecurity.entity.JpaUser;
import com.example.springsecurity.repo.JpaAuthorityRepo;
import com.example.springsecurity.repo.JpaUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Transactional
@Component
public class DataLoader implements CommandLineRunner {
    @Autowired
    private JpaUserRepo userRepo;

    // As we are using UserDetailsService noy manager
    /*
    @Autowired
    private UserDetailsManager userDetailsManager;
    */
    @Autowired
    private JpaAuthorityRepo authorityRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {

        JpaUser requestUser = new JpaUser("admin".trim().toLowerCase(), passwordEncoder.encode("admin"));
        requestUser.setEnabled(1);
        userRepo.save(requestUser);
        /*
        UserRequest request = new UserRequest();
        request.setUsername("admin");
        request.setPassword(passwordEncoder.encode("admin"));
        userDetailsManager.createUser(request);
         */

        System.out.println(userRepo.findAll());
        // System.out.println(authorityRepo.findAll());
    }
}
