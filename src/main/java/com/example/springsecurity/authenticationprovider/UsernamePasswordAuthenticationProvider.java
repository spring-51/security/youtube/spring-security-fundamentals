package com.example.springsecurity.authenticationprovider;

import com.example.springsecurity.authentication.UsernamePasswordAuthentication;
import com.example.springsecurity.service.JpaUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;


@Component
public class UsernamePasswordAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private JpaUserDetailsService userDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public boolean supports(Class<?> authType // type of Authentication (instance of Authentication is arg of authenticate method of AP)
    ) {
        // this tells spring that this AP support UsernamePasswordAuthenticationToken authentication type
        return UsernamePasswordAuthentication.class.equals(authType);
    }

    /**
     * This method is called only if this AP support
     *
     * @param authentication
     * @return
     * @throws AuthenticationException
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        // implement authentication logic here
        // case 1
        // if the request is authenticated successfully you should return here
        // an fully authenticated Authentication instance

        // case 2
        // if the request is authenticated failed, throw Authentication Exception

        // case 3
        /**
         * if authentication is not supported by this AP -> return null
         * since "authenticate" method will only be called after "supports" method return true
         * so here not supported means that this AP can't decide whether the user is valid or not
         * eg. we can have authentication of type UsernamePasswordAuthenticationToken for two AP
         * one AP reads creds from table1, another AP read in memory or from table2
         */

        // TODO: this AP does not support  returning null
        String username = authentication.getName();
        String password = (String)authentication.getCredentials();

        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        if(passwordEncoder.matches(password , userDetails.getPassword())){
            return new UsernamePasswordAuthentication(username, password, Arrays.asList(() ->"role"));
        }
        throw new BadCredentialsException("UsernamePasswordAuthenticationProvider -> throws bad creds ..!!");

    }
}
