package com.example.springsecurity.authenticationprovider;

import com.example.springsecurity.authentication.TokenAuthentication;
import com.example.springsecurity.repo.inmem.TokenDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class TokenAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private TokenDb tokenDb;
    /**
     * This method is called only if this AP support
     *
     * @param authentication
     * @return
     * @throws AuthenticationException
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        // implement authentication logic here
        // case 1
        // if the request is authenticated successfully you should return here
        // an fully authenticated Authentication instance

        // case 2
        // if the request is authenticated failed, throw Authentication Exception

        // case 3
        /**
         * if authentication is not supported by this AP -> return null
         * since "authenticate" method will only be called after "supports" method return true
         * so here not supported means that this AP can't decide whether the user is valid or not
         * eg. we can have authentication of type UsernamePasswordAuthenticationToken for two AP
         * one AP reads creds from table1, another AP read in memory or from table2
         */

        // TODO: this AP does not support  returning null
        String token = authentication.getName();

        if(tokenDb.validate(token)){
            return new TokenAuthentication(token, null, Arrays.asList(() ->"role"));
        }
        throw new BadCredentialsException("TokenAuthenticationProvider -> throws bad creds ..!!");
    }

    @Override
    public boolean supports(Class<?> authType) {
        // this tells spring that this AP support UsernamePasswordAuthenticationToken authentication type
        return TokenAuthentication.class.equals(authType);
    }
}
