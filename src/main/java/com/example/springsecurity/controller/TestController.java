package com.example.springsecurity.controller;

import org.springframework.scheduling.annotation.Async;
import org.springframework.security.concurrent.DelegatingSecurityContextRunnable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestController
public class TestController {

    /**
     *
     * @param authentication - for param of Authentication type
     *                       spring automatically inject it after reading from SecurityContextHolder
     *                       this auto injection works ONLY within CONTROLLERS
     *                       in service layer to get Authentication object use SecurityContextHolder.getContext().getAuthentication()
     * @return
     */
    @GetMapping(value = "/test")
    public String test(Authentication authentication){
        String currentThreadName = Thread.currentThread().getName();
        System.out.println(currentThreadName+" Auto injection::::::"+authentication);
        Authentication fromSCH = SecurityContextHolder.getContext().getAuthentication();
        System.out.println(currentThreadName+" From Security Context ::::::"+ fromSCH);
        String response = "TestController -> test"+" authentication : "+authentication.getName();
        response = response.concat(" from SCH : "+fromSCH.getName());
        return response;
    }

    /**
     *
     * @param authentication - for param of Authentication type
     *                       spring automatically inject it after reading from SecurityContextHolder
     *                       this auto injection works ONLY within CONTROLLERS
     *                       in service layer or child thread to get Authentication object use SecurityContextHolder.getContext().getAuthentication()
     *
     *                       HERE CHILD THREAD IS CREATED BY SPRING DUE TO @Async - CHILD THREAD IS MANAGED BY SPRING
     * @return
     */
    @GetMapping(value = "/testAsync")
    @Async // this tell spring to create a new thread to execute test() method
    // this will only work if we enabled async using @EnableAsync with @SpringBootApplication
    public String testAsync(Authentication authentication){
        String currentThreadName = Thread.currentThread().getName();
        System.out.println(currentThreadName+" Auto injection::::::"+authentication);
        Authentication fromSCH = SecurityContextHolder.getContext().getAuthentication();
        System.out.println(currentThreadName+" From Security Context ::::::"+ fromSCH);
        String response = "TestController -> test"+" authentication : "+authentication.getName();
        response = response.concat(" from SCH : "+fromSCH.getName());
        return response;
    }

    /**
     *
     * @param authentication - for param of Authentication type
     *                       spring automatically inject it after reading from SecurityContextHolder
     *                       this auto injection works ONLY within CONTROLLERS
     *                       in service layer or child thread to get Authentication object use SecurityContextHolder.getContext().getAuthentication()
     *
     *                       HERE CHILD THREAD IS CREATED MANUALLY- CHILD THREAD IS NOT MANAGED BY SPRING
     * @return
     */
    @GetMapping(value = "/testAsync2")
    public String testAsync2(Authentication authentication){
        String currentThreadName = Thread.currentThread().getName();
        System.out.println(currentThreadName+" Auto injection::::::"+authentication);

        Runnable r = () -> {
                Authentication fromSCH = SecurityContextHolder.getContext().getAuthentication();
                System.out.println(Thread.currentThread().getName()+" from runnable  From Security Context ::::::"+ fromSCH);
        };

        /**
         * this is done for manually created thread .
         * this will copy SecurityContext from parent thread to child thread
         */
        // [OPTIONAL from Spring boot version 2.4.5]
        DelegatingSecurityContextRunnable dr = new DelegatingSecurityContextRunnable(r);

        ExecutorService service = Executors.newSingleThreadExecutor();
        service.submit(dr);
        service.shutdown();

        String response = "TestController -> test"+" authentication : "+authentication.getName();
        return response;
    }
}
