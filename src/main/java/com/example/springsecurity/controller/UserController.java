package com.example.springsecurity.controller;

import com.example.springsecurity.dto.UserRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    /**
     * we have defined this bean
     * in ProjectSecurityConfig-> userDetailsService
     */
    // TODO: uncomment this and use something to persist user
    /*
    @Autowired
    private UserDetailsManager userDetailsManager;
     */

    @Autowired
    private PasswordEncoder passwordEncoder;

    // TODO: uncomment this and use something to persist user
    /*
    @PostMapping
    UserRequest create(@RequestBody UserRequest request){
        request.setPassword(passwordEncoder.encode(request.getPassword()));
        userDetailsManager.createUser(request);
        return request;
    }
     */
}
