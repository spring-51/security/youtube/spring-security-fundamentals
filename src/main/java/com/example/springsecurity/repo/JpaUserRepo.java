package com.example.springsecurity.repo;

import com.example.springsecurity.entity.JpaUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface JpaUserRepo extends JpaRepository<JpaUser, Long> {
    @Query(value = "SELECT u FROM JpaUser u WHERE u.username = :un")
    Optional<JpaUser> findByUsername(@Param("un") String username);
}
