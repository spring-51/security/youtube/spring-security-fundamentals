package com.example.springsecurity.repo;

import com.example.springsecurity.entity.JpaAuthority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaAuthorityRepo  extends JpaRepository<JpaAuthority, Long> {
}
