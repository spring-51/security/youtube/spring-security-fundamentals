package com.example.springsecurity.repo.inmem;

import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.Set;

@Repository
public class TokenDb {
    private Set<String> tokens = new HashSet<>();

    public void add(String s){
        tokens.add(s);
    }

    /**
     * if token is present in the set its a vaild token
     * @param s
     * @return
     */
    public Boolean validate(String s){
        return tokens.contains(s);
    }

}
