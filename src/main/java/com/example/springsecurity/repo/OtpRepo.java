package com.example.springsecurity.repo;

import com.example.springsecurity.entity.Otp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OtpRepo extends JpaRepository<Otp,Long> {

    Optional<List<Otp>> findOtpByUsername(String username);
}
