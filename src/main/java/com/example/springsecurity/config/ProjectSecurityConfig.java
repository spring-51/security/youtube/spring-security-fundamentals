package com.example.springsecurity.config;

import com.example.springsecurity.authenticationprovider.TokenAuthenticationProvider;
import com.example.springsecurity.authenticationprovider.UsernameOtpAuthenticationProvider;
import com.example.springsecurity.authenticationprovider.UsernamePasswordAuthenticationProvider;
import com.example.springsecurity.filter.TokenAuthenticationFilter;
import com.example.springsecurity.filter.UsernamePasswordAuthFilter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
public class ProjectSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UsernameOtpAuthenticationProvider usernameOtpAuthenticationProvider;

    @Autowired
    private UsernamePasswordAuthenticationProvider usernamePasswordAuthenticationProvider;

    /*
    @Autowired
    private DataSource dataSource;
     */

    @Autowired
    private TokenAuthenticationProvider tokenAuthenticationProvider;

    /**
     * if this UserDetailsService bean is not defined,
     * spring sec auto config kicks to create it with  default
     * user with username as "user" and password will
     * be generated during bootstrap and logs it in console
     *
     * ##############################################
     * We can avoid creating this bean if we are overriding
     * config method of Spring Sec
     * ##############################################
     *
     * ##############################################
     * if we define this bean then we MUST need to create bean of
     * passwordEncoder OR use SpEl as ${noop}.
     * Here we are creating bean of passwordEncoder
     * ##############################################
     * @return
     */
    // @Bean // this is commented since from L6 we are using @Service on JpaUserDetailsService
    @Deprecated // since from L6 we are using @Service on JpaUserDetailsService
    public UserDetailsService userDetailsService(){
        //In memory user details service
        InMemoryUserDetailsManager uds = new InMemoryUserDetailsManager();

        UserDetails userDetails = User.withUsername("bill")
                .password(passwordEncoder().encode("bill"))
                .authorities("read").build();

        uds.createUser(userDetails);
        return uds;

        // user details service from h2 db
        // return  new JpaUserDetailsService();

        // jdbc user details manager
        /**
         * if we uses JdbcUserDetailsManager then we need to follow table names as per
         * defined in JdbcUserDetailsManager refer **JdbcUserDetailsManager**
         */
         // return new JdbcUserDetailsManager(dataSource);
    }

    /**
     * Password Encoder bean
     * IT'S a MUST when we create our own bean of
     * UserDetailsService
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder(){
        //return NoOpPasswordEncoder.getInstance();
        /**
         * Advantage of using Bcrypt
         * 1. - we can't extract password back from encoded string.
         * 2. - for same password string encoded string is always different
         */
        return new BCryptPasswordEncoder();
    }

    /**
     * to congif whitelistin api
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic();
        http.csrf().disable(); // disable CSRF TOKEN , by default its MUST for all the request other than GET
        http.authorizeRequests()
                .mvcMatchers("/users").permitAll()
                .anyRequest().authenticated();

        /**
         * for filter create bea using @Bean ,
         * AVOID Stereotype annotation(But why ?)
         * */
        http.addFilterAt(usernamePasswordAuthFilter(), BasicAuthenticationFilter.class); // it tells spring that add customAuthenticationFilter in place of BasicAuthenticationFilter
        http.addFilterAfter(tokenAuthenticationFilter(), BasicAuthenticationFilter.class);
    }

    /**
     * too config custom AuthenticationProvider etc.
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth){

        auth
                .authenticationProvider(usernamePasswordAuthenticationProvider) // adding 1st authentication provider
                .authenticationProvider(usernameOtpAuthenticationProvider) // adding 2nd authentication provider
                .authenticationProvider(tokenAuthenticationProvider) // adding 3rd
        ;
    }

    /**
     * this is MUST if we need AuthenticationManager bean in the app.
     * like in filters / controllers / services
     *
     * We are using this in CustomAuthenticationFilter
     *
     * @return
     * @throws Exception
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public UsernamePasswordAuthFilter usernamePasswordAuthFilter(){
        return new UsernamePasswordAuthFilter();
    }

    @Bean
    public TokenAuthenticationFilter tokenAuthenticationFilter(){
        return new TokenAuthenticationFilter();
    }

    @Bean
    public InitializingBean initializingBean(){
        return () ->{
            SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
        };
    }
}
