package com.example.springsecurity.entity;

import javax.persistence.*;

@Entity
@Table(name = "authorities")
public class JpaAuthority {
    @Id
    private String username;
    private String authority;

    public JpaAuthority() {
    }

    public JpaAuthority(String username, String authority) {
        this.username = username;
        this.authority = authority;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String toString() {
        return "JpaAuthority{" +
                ", username='" + username + '\'' +
                ", authority='" + authority + '\'' +
                '}';
    }
}
