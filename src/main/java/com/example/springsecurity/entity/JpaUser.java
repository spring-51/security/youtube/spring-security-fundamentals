package com.example.springsecurity.entity;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class JpaUser {
    @Id
    private String username;
    private String password;
    private Integer enabled; // TODO: correct data type once we have finalised it

    public JpaUser() {
    }

    public JpaUser(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "JpaUser{" +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", enabled=" + enabled +
                '}';
    }
}
