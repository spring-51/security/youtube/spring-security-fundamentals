package com.example.springsecurity.authentication;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
/**
 * this class will be used in Custom AP and CustomFilter
 *
 * to extend UsernamePasswordAuthenticationToken is not good practice create Authentication type
 * prefer implementing Authentication over this
 */
public class UsernameOtpAuthentication extends UsernamePasswordAuthenticationToken {
    public UsernameOtpAuthentication(Object principal, Object credentials) {
        super(principal, credentials);
    }

    public UsernameOtpAuthentication(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
    }
}
