package com.example.springsecurity.service;

import com.example.springsecurity.dto.JpaUserDetails;
import com.example.springsecurity.entity.JpaUser;
import com.example.springsecurity.repo.JpaUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JpaUserDetailsService implements UserDetailsService {

    @Autowired
    private JpaUserRepo repo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        JpaUser jpaUser = repo.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(" Error ..!! user not found"));

        return new JpaUserDetails(jpaUser);
    }
}
