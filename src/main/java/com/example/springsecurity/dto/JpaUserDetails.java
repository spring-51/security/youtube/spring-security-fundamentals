package com.example.springsecurity.dto;

import com.example.springsecurity.entity.JpaUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;

/**
 * We could have used JpaUser.
 * But it will conflict Single Responsibility Principle.
 *
 * JpaUserDetails is child of Spring UserDetails and will be used
 * ONLY in JpaUserDetailsService
 *
 */
public class JpaUserDetails implements UserDetails {

    private final JpaUser user;

    public JpaUserDetails(JpaUser user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        //TODO: we have hardcoded this for now
        // This will come from another table in future
        return Arrays
                // this lambda is for GrantedAuthority
                .asList(() -> "read");
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
